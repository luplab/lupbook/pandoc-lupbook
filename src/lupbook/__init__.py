# Copyright (c) 2024 LupLab
# SPDX-License-Identifier: AGPL-3.0-or-later

# Copyright (c) 2024 LupLab
# SPDX-License-Identifier: AGPL-3.0-or-later

import argparse
from importlib.resources import files
import os
import shutil
import subprocess

def validate_input_path(path):
    if not os.path.exists(path):
        raise argparse.ArgumentTypeError(f"Path does not exist: {path}")
    if not os.access(path, os.R_OK):
        raise argparse.ArgumentTypeError(f"Path is not readable: {path}")
    return path

def validate_input_dir(path):
    path = validate_input_path(path)
    if not os.path.isdir(path):
        raise argparse.ArgumentTypeError(f"Path is not a directory: {path}")
    return path

def validate_input_file(path):
    path = validate_input_path(path)
    if not os.path.isfile(path):
        raise argparse.ArgumentTypeError(f"Path is not a file: {path}")
    return path

def validate_output_path(path):
    # Check if path exists
    if os.path.exists(path):
        # If it exists, check if it's writable
        if not os.access(path, os.W_OK):
            raise argparse.ArgumentTypeError(f"Path exists but is not writable: {path}")
    else:
        # If it doesn't exist, check if parent directory is writable
        parent = os.path.dirname(path) or '.'
        if not os.access(parent, os.W_OK):
            raise argparse.ArgumentTypeError(
                f"Parent directory is not writable: {parent}")
    return path

def validate_pandoc():
    try:
        subprocess.run(["pandoc", "--version"], check=True, capture_output=True)
        return "pandoc"
    except (subprocess.SubprocessError, FileNotFoundError):
        raise RuntimeError("pandoc is not available in PATH")

def mkdir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)

class BookBuilder:
    def __init__(self, args):
        # Figure out our pandoc-lupbook directory based on where this file is
        self.lupbook_dir = files("lupbook")
        assets_dir = self.lupbook_dir.joinpath("assets")
        filters_dir = self.lupbook_dir.joinpath("filters")

        # TODO: make this an option
        self.template_file = str(assets_dir.joinpath("template", "default.html"))
        validate_input_file(self.template_file)

        # Textbook source code
        self.src_dir = args.src_dir or os.getcwd()

        # VM file
        # TODO be able to use the latest artifact online
        self.lupbookvmjs_file = args.lupbookvmjs_file or \
                os.path.join(self.src_dir, "lupbookvm.js")
        validate_input_file(self.lupbookvmjs_file)

        # Pandoc + filters
        self.pandoc = validate_pandoc()
        self.filters = []
        for f in ["lupbook.py", "toc_filter.py"]:
            self.filters.append(str(filters_dir.joinpath(f)))
            validate_input_file(self.filters[-1])

        # Build info
        self.build_dir = args.build_dir or self.src_dir
        validate_output_path(self.build_dir)

        build_file = args.build_file or "book.html"
        self.build_path = os.path.join(self.build_dir, build_file)

    def build_book(self):
        self._prep()
        self._show()
        self._build()
        print("*** Artifact ***")
        print(f"file://{self.build_path}")

    def _prep(self):
        # Ensure build directory exists
        mkdir(self.build_dir)

    def _show(self):
        print("*** Configuration ***")
        print(f"Source directory: {self.src_dir}")
        print(f"Build file: {self.build_path}")
        print(f"Lupbook directory: {self.lupbook_dir}")
        print(f"VM file: {self.lupbookvmjs_file}")

    def _build(self):
        print("*** Build ***")

        # Construct pandoc command
        cmd = [
                self.pandoc,
                "-o", self.build_path,
                "-V", f"lbdir={self.lupbook_dir}",
                "-V", f"lbvm={self.lupbookvmjs_file}",
                "--embed-resources",
                "--standalone",
                "--section-divs",
                "--template", self.template_file
                ]

        # Add filters
        for f in self.filters:
            cmd.extend(["--filter", f])

        # Add markdown files (sorted for consistent order)
        md_files = [f for f in os.listdir(self.src_dir) if f.endswith(".md")]
        if not md_files:
            raise RuntimeError("No markdown files found in source directory")
        md_files.sort()
        cmd.extend(md_files)

        # Execute pandoc command
        print(" ".join(cmd))
        try:
            subprocess.run(cmd, check=True)
        except subprocess.CalledProcessError as e:
            raise RuntimeError(f"Pandoc command failed: {e}")

def main():
    parser = argparse.ArgumentParser(
            description = "Build interactive textbooks")

    parser.add_argument("--src-dir",
                        metavar='DIRECTORY',
                        type=validate_input_dir,
                        help="Textbook source directory. "
                        "Default is current directory.")
    parser.add_argument("--lupbookvmjs-file",
                        metavar='FILE',
                        type=validate_input_file,
                        help="Specify custom lupbookvm.js file. "
                        "Default is to use file `lupbookvm.js` in textbook source directory.")
    parser.add_argument("--build-dir",
                        metavar='DIRECTORY',
                        type=validate_output_path,
                        help="Build directory. Default is current directory.")
    parser.add_argument("--build-file", default="book.html",
                        metavar='FILE',
                        help="Output HTML file name. Default is %(default)s.")

    try:
        args = parser.parse_args()
        builder = BookBuilder(args)
        builder.build_book()
    except (argparse.ArgumentTypeError, RuntimeError) as e:
        parser.error(str(e))

if __name__ == "__main__":
    main()
