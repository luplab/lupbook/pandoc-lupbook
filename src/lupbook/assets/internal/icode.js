/*
 * Copyright (c) 2021 LupLab
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

class IcodeTest {
  static RESULT = Object.freeze({
    PASS: "PASS",
    FAIL: "FAIL",
    FATAL: "FATAL"
  });

  data = {};

  itemElt;
  btnElt;
  bodyElt;
  bodyCollapse;
  scoreElt;
  outputElt;

  constructor(idx, elt, icodePrefixId) {
    /* Get test parameters from the DOM */
    Object.assign(this.data, JSON.parse(atob(elt.dataset.params)));

    const testId = `${icodePrefixId}-test-${idx}`;

    this.itemElt = elt;
    this.btnElt = document.getElementById(`${testId}-btn`);
    this.bodyElt = document.getElementById(`${testId}`);
    this.bodyCollapse = new bootstrap.Collapse(this.bodyElt, {
      toggle: false
    });
    this.scoreElt = document.getElementById(`${testId}-score`);
    this.outputElt = document.getElementById(`${testId}-output`);

    /* XXX: this shouldn't be necessary because we're already generating
     * disabled buttons in the python filter but Firefox somehow removes the
     * disabled attributes for some accordions */
    this.btnElt.disabled = true;
  }

  reset() {
    /* Clear any visually displayed results */
    this.scoreElt.textContent = "";
    this.scoreElt.classList.remove("alert-danger", "alert-success");
    this.outputElt.textContent = "";
    this.btnElt.firstElementChild.classList.remove(
      "bi-check-circle-fill",
      "bi-x-circle-fill",
      "text-success",
      "text-danger"
    );
    this.btnElt.firstElementChild.classList.add(
      "bi-dash-circle-fill",
      "text-secondary"
    );
    /* Hide everything */
    this.btnElt.disabled = true;
    this.bodyCollapse.hide();
  }

  async run(sessionId) {
    let ret;

    /* Run test */
    const res = await LupBookVM.Session.exec(sessionId, this.data.cmd);

    /* Show output in accordion body */
    this.outputElt.textContent = `# ${this.data.cmd}\n${res.outputCmd}`;

    /* Exit code must be 0 for test to pass */
    if (res.exitCode !== 0) {
      /* Testing score */
      this.scoreElt.classList.add("alert-danger");
      this.scoreElt.textContent = `Test failed: exit code ${res.exitCode}`;

      /* Accordion header */
      this.btnElt.firstElementChild.classList.add(
        "bi-x-circle-fill",
        "text-danger"
      );

      if (this.data.fatal) ret = IcodeTest.RESULT.FATAL;
      else ret = IcodeTest.RESULT.FAIL;
    } else {
      /* Testing score */
      this.scoreElt.classList.add("alert-success");
      this.scoreElt.textContent = "Test passed";

      /* Accordion header */
      this.btnElt.firstElementChild.classList.add(
        "bi-check-circle-fill",
        "text-success"
      );

      ret = IcodeTest.RESULT.PASS;
    }

    this.btnElt.disabled = false;
    return ret;
  }

  show() {
    /* Scroll whole accordion item into view once the accordion body is shown */
    this.bodyElt.addEventListener(
      "shown.bs.collapse",
      () => this.itemElt.scrollIntoView({ block: "nearest" }),
      { once: true }
    );
    /* Show accordion body */
    this.bodyCollapse.show();
  }
}

class IcodeActivity extends LupBookActivity {
  /* Class members */
  srcFiles = {};
  srcFileElements;

  tests = [];

  sessionId = null;
  forceUpload;

  cmInstances = new Map();
  editableSections = new Map();

  editorChangeEventHandler;
  debounceTimer;
  debounceCounter = 0;

  static savingConfig = Object.freeze({
    /* Wait 2 seconds after last keystroke before saving */
    DEBOUNCE_INTERVAL: 2000,
    /* Number of skipped debounce periods before forcefully saving */
    FORCE_SAVE_INTERVAL_COUNT: 10
  });

  debugBtn;

  /* Class constructor */
  constructor(elt) {
    super("icode", elt);

    this.srcFileElements = Array.from(
      elt.getElementsByClassName("icode-srcfile")
    );

    /* Create test objects */
    Array.from(elt.getElementsByClassName("icode-test")).forEach(
      (testElt, idx) =>
        this.tests.push(new IcodeTest(idx, testElt, this.prefixId))
    );

    this.debugBtn = document.getElementById(`${this.prefixId}-debug`);
  }

  async initActivity() {
    await super.initActivity();

    /* Upon editor changes */
    this.editorChangeEventHandler = (() => {
      /* Re-enable submission button */
      this.submitStatus(LupBookActivity.SubmitStatus.ENABLED);
      /* Initiate saving action */
      this.debouncedSave();
    }).bind(this);

    this.debugBtn.onclick = () => this.onDebug();

    /*
     * Source file editor
     */
    /* Code mirror generic arguments */
    const cmBaseArgs = {
      lineNumbers: true,
      matchBrackets: true,
      indentUnit: 4,
      mode: "text/x-csrc" /* TODO: auto-detection of mode using mode/meta.js addon */,
      extraKeys: {
        Tab: (cm) => cm.execCommand("indentMore"),
        "Shift-Tab": (cm) => cm.execCommand("indentLess")
      }
    };

    this.srcFileElements.forEach((srcFileElt) => {
      /* Create editor instance */
      const cm = CodeMirror.fromTextArea(srcFileElt, cmBaseArgs);

      /* Callbacks to editor used in other methods */
      const filename = srcFileElt.dataset.filename;
      this.srcFiles[filename] = {
        docInit: cm.getDoc().copy(),
        resetDoc: function () {
          cm.swapDoc(this.docInit.copy());
        },
        getData: () => cm.getValue(),
        isClean: () => cm.isClean(),
        markClean: () => cm.markClean()
      };

      /* That's all we need to setup for hidden files */
      if (srcFileElt.dataset.hidden === "data-hidden") return;

      /* Theming */
      const setTheme = () => {
        cm.setOption(
          "theme",
          document.documentElement.getAttribute("data-bs-theme") === "light"
            ? "default"
            : "monokai"
        );
      };
      /* Set theme immediately and keep it updated wrt system's preferences */
      setTheme();
      const themeObserver = new MutationObserver(setTheme);
      themeObserver.observe(document.documentElement, {
        attributes: true,
        attributeFilter: ["data-bs-theme"]
      });

      /* Refresh editor when tab is shown (when it was previously hidden) */
      const tab = document.getElementById(srcFileElt.dataset.tab);
      tab.addEventListener("shown.bs.tab", () => cm.refresh());
      /* And when section is shown from clickin in the TOC */
      this.sectionDiv.addEventListener("shown.lb.section", () => cm.refresh());

      /* Handle submit/reset buttons upon file changes */
      cm.on("changes", this.editorChangeEventHandler);

      /* Set up specific appearance and saving regions based on file mode */
      switch (srcFileElt.dataset.readonly) {
        case undefined:
          this.initFileWritable(srcFileElt, cm);
          break;
        case "data-readonly":
          this.initFileReadonly(srcFileElt, cm);
          break;
        default:
          this.initFilePartial(srcFileElt, cm);
          break;
      }
    });

    /* Load existing saved data if any */
    await this.loadActivity();

    this.resetStatus(true);
  }

  initFileReadonly(srcFileElt, cm) {
    /* Prevent from writing */
    cm.setOption("readOnly", true);
    /* Gray out all the lines */
    cm.eachLine(0, cm.lineCount(), (line) => {
      cm.addLineClass(line, "background", "bg-body-secondary");
    });
    /* Note that there is not regions to track for saving in RO files */
  }

  initFilePartial(srcFileElt, cm) {
    /* Unique handle on file */
    const cmTab = srcFileElt.dataset.tab;

    /* Bind to editor */
    this.cmInstances.set(cmTab, cm);

    /* Note that RO ranges we get from user config are 1-indexed, so ranges
     * typically need to be adjusted by `-1` below */
    const readOnlyRanges = JSON.parse(atob(srcFileElt.dataset.readonly));

    /* Mark RO ranges as non-writeable in editor and gray them out */
    readOnlyRanges.forEach((range) => {
      cm.markText(
        { line: range[0] - 2 },
        { line: range[1], ch: 0 },
        {
          readOnly: true,
          inclusiveLeft: range[0] === 1,
          inclusiveRight: range[1] === cm.lineCount()
        }
      );
      cm.eachLine(range[0] - 1, range[1], (line) => {
        cm.addLineClass(line, "background", "bg-body-secondary");
      });
    });

    /* Now determine the ranges of writable lines to keep track of for saving
     * purposes */
    let editableRanges = [];
    this.getEditableRanges(readOnlyRanges, cm.lineCount()).forEach((range) => {
      editableRanges.push(
        cm.markText(
          { line: range[0] - 1, ch: 0 },
          { line: range[1] - 1 },
          {
            readOnly: false,
            inclusiveLeft: true,
            inclusiveRight: true,
            clearOnEnter: false,
            clearWhenEmpty: false
          }
        )
      );
    });
    this.editableSections.set(cmTab, editableRanges);
  }

  initFileWritable(srcFileElt, cm) {
    /* Unique handle on file */
    const cmTab = srcFileElt.dataset.tab;

    /* Bind to editor */
    this.cmInstances.set(cmTab, cm);

    /* Track entire file for saving */
    const sectionMarker = cm.markText(
      { line: 0 },
      { line: cm.lineCount() },
      {
        readOnly: false,
        inclusiveLeft: true,
        inclusiveRight: true,
        clearOnEnter: false,
        clearWhenEmpty: false
      }
    );
    this.editableSections.set(cmTab, [sectionMarker]);
  }

  /* Returns sorted editable ranges, and keep 1-indexed line numbers from user
   * config */
  getEditableRanges(readOnlyRanges, lineCount) {
    /* Shouldn't happen, but RO range is empty */
    if (readOnlyRanges == null) return [[1, lineCount]];

    /* Each section before an RO range is an editable range */
    let start = 1,
      editableRanges = [];
    readOnlyRanges.forEach((range) => {
      if (start < range[0]) editableRanges.push([start, range[0] - 1]);
      start = range[1] + 1;
    });
    /* Possible editable range after the last RO range of the file */
    if (start <= lineCount) editableRanges.push([start, lineCount]);

    return editableRanges;
  }

  debouncedSave() {
    /* Cancel current timer if any and keep track of the number of skipped
     * debounce periods */
    if (this.debounceTimer) {
      clearTimeout(this.debounceTimer);
      this.debounceCounter++;
    }

    /* Trigger a forced save after we've skipped a specific amount of debounce
     * periods, otherwise start another debounce before saving */
    if (
      this.debounceCounter >=
      IcodeActivity.savingConfig.FORCE_SAVE_INTERVAL_COUNT
    ) {
      this.saveActivity();
    } else {
      this.debounceTimer = setTimeout(() => {
        this.saveActivity();
      }, IcodeActivity.savingConfig.DEBOUNCE_INTERVAL);
    }
  }

  async saveActivity() {
    let jsonDataArray = [];

    this.editableSections.forEach((fileEditableSections, cmTab) => {
      const cm = this.cmInstances.get(cmTab);

      let fileTextRanges = [];
      fileEditableSections.forEach((editableSection) => {
        const cmRange = editableSection.find();
        const textRange = cm.getRange(cmRange.from, cmRange.to);
        fileTextRanges.push(textRange);
      });

      jsonDataArray.push({
        id: cmTab,
        ranges: fileTextRanges
      });
    });

    await this.saveData({
      id: this.prefixId,
      files: jsonDataArray
    });

    /* Reset debounce logic */
    clearTimeout(this.debounceTimer);
    this.debounceTimer = null;
    this.debounceCounter = 0;
  }

  async loadActivity() {
    const data = await this.loadData();
    if (!data) return;

    /* Restore each editable section */
    this.editableSections.forEach((fileEditableSections, cmTab) => {
      const cm = this.cmInstances.get(cmTab);

      const fileTextRanges = data.files.find(
        (file) => file.id === cmTab
      ).ranges;

      fileEditableSections.forEach((editableSection, idx) => {
        const cmRange = editableSection.find();

        /* Turn off change event handler to prevent the restoring operation
         * from triggering a spurious save that would make us loop */
        cm.off("changes", this.editorChangeEventHandler);
        cm.replaceRange(fileTextRanges[idx], cmRange.from, cmRange.to);
        cm.on("changes", this.editorChangeEventHandler);
      });
    });
  }

  async onDebug() {
    await this.openSession();
    await this.uploadSrcFiles();
    await LupBookVM.Session.debug(this.sessionId, true);

    const termElt = document.getElementById("lbvm-terminal-modal");
    termElt.addEventListener(
      "hide.bs.modal",
      async () => {
        await LupBookVM.Session.debug(this.sessionId, false);
        await this.closeSession();
      },
      { once: true }
    );
    const termModal = new bootstrap.Modal(termElt);
    termModal.show();
  }

  toggleDebug(enabled) {
    this.debugBtn.disabled = !enabled;
  }

  toggleSubmit(enabled) {
    let s = enabled
      ? LupBookActivity.SubmitStatus.ENABLED
      : LupBookActivity.SubmitStatus.DISABLED;
    this.submitStatus(s);
    this.toggleDebug(enabled);
  }

  /* Transfer files associated with icode activity to the VM */
  async uploadSrcFiles() {
    const promises = Object.entries(this.srcFiles).map(
      async ([filename, srcFile]) => {
        /* Skip uploading if file hasn't been modified since the last submit and
         * it's not the first upload */
        if (!this.forceUpload && srcFile.isClean()) return;

        await LupBookVM.Session.upload(
          this.sessionId,
          filename,
          srcFile.getData()
        );

        srcFile.markClean();
      }
    );

    await Promise.all(promises);

    /* Now the VM has a version of every file for this activity. We can rely on
     * whether or not files have been modified, via CodeMirror */
    this.forceUpload = false;
  }

  /* Event handler for submit button */
  async onSubmit() {
    let testFail = null;

    /* Disable buttons */
    this.setResetSubmitStatus(false);
    this.toggleDebug(false);

    /* Clear and show progress bar */
    this.clearProgress();
    this.visibilityProgress(true);

    /* Reset testing area */
    this.resetTesting();

    /* Open session if one doesn't already exist */
    await this.openSession();

    /* Upload the files to the VM */
    await this.uploadSrcFiles();

    /* Run tests one by one */
    for (const [idx, test] of this.tests.entries()) {
      this.progressStatus(idx, LupBookActivity.ProgressStatus.PENDING);

      const res = await test.run(this.sessionId);

      if (res === IcodeTest.RESULT.PASS) {
        this.progressStatus(idx, LupBookActivity.ProgressStatus.SUCCESS);
      } else {
        this.progressStatus(idx, LupBookActivity.ProgressStatus.FAILURE);

        /* Capture which test failed first */
        if (!testFail) testFail = test;

        /* Stop testing if a failed test was indicated as fatal */
        if (res === IcodeTest.RESULT.FATAL) break;
      }
    }

    /* Show feedback and scroll to first failed test if any */
    this.showFeedback();
    if (testFail) testFail.show();

    /* Close session if all tests passed */
    if (!testFail) await this.closeSession();

    /* Overall feedback via submit button */
    let s = testFail
      ? LupBookActivity.SubmitStatus.FAILURE
      : LupBookActivity.SubmitStatus.SUCCESS;
    this.submitStatus(s);
    this.resetStatus(true);
    this.toggleDebug(true);
  }

  /* Event handler for reset button */
  async onReset() {
    /* Reset code */
    Object.values(this.srcFiles).forEach((srcFile) => {
      srcFile.resetDoc();
    });

    await this.closeSession();

    /* Reset testing area */
    this.visibilityProgress(false);
    this.hideFeedback(false);

    this.resetTesting();

    /* Allow new submission */
    this.toggleSubmit(true);
  }

  resetTesting() {
    this.tests.forEach((test) => test.reset());
  }

  async openSession() {
    if (!this.sessionId) {
      this.sessionId = await LupBookVM.Session.open(this.prefixId);
      this.forceUpload = true;
    }
  }

  async closeSession() {
    if (this.sessionId) {
      await LupBookVM.Session.close(this.sessionId);
      this.sessionId = null;
    }
  }
}

/*
 * Initialization of icode activities after DOM loading
 */
window.addEventListener("DOMContentLoaded", async () => {
  /*
   * Set up terminal once
   */

  /* Creation */
  const term = new Terminal({
    scrollback: 10000,
    fontFamily: "monospace",
    cursorBlink: true
  });
  const fitAddon = new FitAddon.FitAddon();
  term.loadAddon(fitAddon);

  /* Attach to container */
  const termElt = document.getElementById("lbvm-terminal");
  term.open(termElt);
  fitAddon.fit();

  const termResize = () => {
    fitAddon.fit();
    LupBookVM.Core.consoleResize();
  };

  /* Set font size as the equivalent of "smaller" */
  const fontSize = parseFloat(window.getComputedStyle(termElt).fontSize) * 0.8;
  term.options.fontSize = fontSize;

  /* Trigger a  resize first time modal is shown (otherwise there's a weird bug
   * where the scrollbar doesn't appear when the modal is shown for the first
   * time because output was added to a non-visible terminal) */
  document
    .getElementById("lbvm-terminal-modal")
    .addEventListener("shown.bs.modal", termResize, { once: true });

  /* Automatically resize if the window gets dynamically resized */
  window.onresize = termResize;

  /* Bind terminal input to VM */
  term.onKey((event) => {
    LupBookVM.Core.consoleQueue(event.key.charCodeAt(0));
  });

  /*
   * Create icode activites
   */
  let icodeActivities = [];
  for (const e of document.getElementsByClassName("icode-container")) {
    const icodeActivity = new IcodeActivity(e);
    await icodeActivity.initActivity();
    icodeActivities.push(icodeActivity);
  }

  /*
   * Run Virtual Machine
   */
  LupBookVM.Client.boot({
    consoleWriteDebug: (str) => term.write(str),
    consoleGeometryDebug: () => {
      return { width: term.cols, height: term.rows };
    }
  })
    .then(() => {
      return LupBookVM.Session.init();
    })
    .then(() => {
      /* Ready to open sessions and handle submissions */
      icodeActivities.forEach((icode) => icode.toggleSubmit(true));
    })
    .catch((err) => console.error("VM Error: " + err));
});
