class LupBookDB {
  /* Singleton instance for use by all activity instances */
  static instance = null;
  static connectionPromise = null;

  dbName;
  dbVersion = 1; /* Increment only in case of significant schema change */
  dbObjectStores = [
    "icode",
    "fib",
    "parsons",
    "matching",
    "mcq",
    "hparsons",
    "settings"
  ];

  dbConnection;

  /* Constructor should only be called by LupBookDB.getDatabase() */
  constructor() {
    if (LupBookDB.instance) return LupBookDB.instance;

    LupBookDB.instance = this;
    /* TODO find a less hacky way to grab the book's title (or create a book ID
     * independent from the title) */
    this.dbName = document
      .getElementById("lb-name")
      .textContent.replaceAll(" ", "");
  }

  /*
   * Initializes the DB instance and opens a connection.Returns a connected
   * singleton instance of the DB or throws error on failure.
   */
  static async getDatabase() {
    if (LupBookDB.connectionPromise) {
      await LupBookDB.connectionPromise;
      return LupBookDB.instance;
    }

    if (!LupBookDB.instance) new LupBookDB();

    LupBookDB.connectionPromise = new Promise((resolve, reject) => {
      /*
       * Request permission for persistent storage. Note: Many Chromium-based
       * browsers and some other browsers auto-reject such requests and maintain
       * their own rules and criteria for IndexedDB persistence.
       */
      let isPersistent = false;
      if (navigator.storage && navigator.storage.persist)
        navigator.storage.persisted().then((persisted) => {
          isPersistent = persisted;
        });

      const request = indexedDB.open(
        LupBookDB.instance.dbName,
        LupBookDB.instance.dbVersion
      );

      request.onupgradeneeded = (event) => {
        const db = event.target.result;
        LupBookDB.instance.dbObjectStores.forEach((storeName) => {
          if (!db.objectStoreNames.contains(storeName))
            db.createObjectStore(storeName, { keyPath: "id" });
        });
      };

      request.onsuccess = (event) => {
        LupBookDB.instance.connectionStatus(true, isPersistent);
        LupBookDB.instance.installImportExport();
        resolve(event.target.result);
      };

      request.onerror = (event) => {
        reject(event.target.error);
      };
    });

    try {
      LupBookDB.instance.dbConnection = await LupBookDB.connectionPromise;
    } catch (error) {
      LupBookDB.instance.connectionStatus(false);
      throw new Error("DB connection failed", { cause: error });
    }

    return LupBookDB.instance;
  }

  connectionStatus(success, isPersistent = false) {
    const statusIndicator = document.getElementById("lb-saving-indicator");
    const loadingIcon = document.getElementById("lb-saving-loading");
    const successIcon = document.getElementById("lb-saving-success");
    const errorIcon = document.getElementById("lb-saving-error");
    const statusBadge = document.getElementById("lb-saving-badge");

    loadingIcon.classList.add("d-none");

    if (success) {
      successIcon.classList.remove("d-none");
      if (isPersistent) {
        statusBadge.classList.add("d-none");
        statusIndicator.setAttribute("title", "Your progress is being saved!");
      } else {
        statusIndicator.setAttribute(
          "title",
          "Your progress is being saved!" +
            " However, it is not guaranteed to be persistent by your web browser." +
            " You may want to export your data regularly."
        );
      }
    } else {
      window.alert(
        "Error: connection to local database failed!\n\n" +
          "We are unable to save/restore your progress at the moment."
      );
      errorIcon.classList.remove("d-none");
      statusIndicator.setAttribute(
        "title",
        "Your progress is not being saved!"
      );
    }
  }

  installImportExport() {
    /* Importing */
    const importFileInput = document.getElementById("import-data-input");
    importFileInput.addEventListener("change", (event) => {
      const file = event.target.files[0];
      /* Error checking */
      if (file.type !== "application/json" || !file.name.endsWith(".json")) {
        window.alert("Error: selected file is not a JSON file.");
        return;
      }
      /* Import content */
      this.importData(file)
        .then(() => window.location.reload())
        .catch((event) => window.alert(event.target.error));
    });

    /* Exporting */
    const exportBtn = document.getElementById("export-data-button");
    exportBtn.addEventListener("click", () => this.exportData());
  }

  /*
   * Adds/updates data for an activity instance.
   * @param {string} storeName - Activity type
   * @param {Object} data - Data to be saved, representated as a key-value
   *   JSON object: { id: <prefixId>, ... }
   * @throws {Error} In case of failure
   */
  async saveData(storeName, data) {
    if (this.dbConnection === null)
      throw new Error("Database is not connected");

    if (!this.dbObjectStores.includes(storeName))
      throw new Error(`Object store ${storeName} not found`);

    if (!data.id) throw new Error("Data must have an 'id' key");

    try {
      const store = this.dbConnection
        .transaction(storeName, "readwrite")
        .objectStore(storeName);
      await new Promise((resolve, reject) => {
        const request = store.put(data);
        request.onsuccess = () => resolve();
        request.onerror = (event) => reject(event.target.error);
      });
    } catch (error) {
      throw new Error("Data saving failed", { cause: error });
    }
  }

  /*
   * Loads data for an activity instance.
   * @param {string} storeName - activity type
   * @param {string} activityId - activity's unique identifier (typically its
   *   prefixId)
   * @returns {Object} Loaded data
   * @throws {Error} In case of failure
   */
  async loadData(storeName, activityId) {
    if (this.dbConnection === null)
      throw new Error("Database is not connected");

    if (!this.dbObjectStores.includes(storeName))
      throw new Error(`Object store ${storeName} not found`);

    try {
      const response = await new Promise((resolve, reject) => {
        const store = this.dbConnection
          .transaction(storeName, "readwrite")
          .objectStore(storeName);
        const request = store.get(activityId);
        request.onsuccess = (event) => resolve(event.target.result);
        request.onerror = (event) => reject(event.target.error);
      });
      return response;
    } catch (error) {
      throw new Error("Data loading failed", { cause: error });
    }
  }

  /*
   * Export data from all object stores as one big JSON file.
   */
  async exportData() {
    if (this.dbConnection === null)
      throw new Error("Database is not connected");

    const transaction = this.dbConnection.transaction(
      this.dbObjectStores,
      "readonly"
    );

    const promises = this.dbObjectStores.map((storeName) => {
      const store = transaction.objectStore(storeName);

      return new Promise((resolve, reject) => {
        const request = store.getAll();
        request.onsuccess = (event) =>
          resolve({ [storeName]: event.target.result });
        request.onerror = (event) => reject(event.target.error);
      });
    });

    /* Get all the data */
    let data;
    try {
      const results = await Promise.all(promises);
      /* Merge array of objects into one object where keys are the store names */
      data = Object.assign({}, ...results);
    } catch (error) {
      throw new Error("Data exporting failed", { cause: error });
    }

    const blob = new Blob([JSON.stringify(data)], { type: "application/json" });
    const url = URL.createObjectURL(blob);
    const currentDate = new Date().toISOString();

    /* Create temporary link and click on it to initiate download */
    const a = document.createElement("a");
    a.href = url;
    a.download = `${this.dbName}_export_${currentDate}.json`;
    document.body.appendChild(a);
    a.click();

    /* Clean up after download */
    document.body.removeChild(a);
    URL.revokeObjectURL(url);
  }

  /*
   * Import data from user file
   * @param {File} file - file input from user
   * @throws {Error} In case of failure
   */
  async importData(file) {
    if (this.dbConnection === null)
      throw new Error("Database is not connected");

    /* Read file */
    const fileContent = await new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (event) => resolve(event.target.result);
      reader.onerror = () =>
        reject(new Error("Failed to read file", { cause: event.target.error }));
      reader.readAsText(file);
    });

    /* Parse file as JSON */
    let parsedData;
    try {
      parsedData = JSON.parse(fileContent);
    } catch (error) {
      throw new Error("Failed to parse JSON", { cause: error });
    }

    const transaction = this.dbConnection.transaction(
      this.dbObjectStores,
      "readwrite"
    );

    /* Array of promises for each DB store */
    const promises = [];
    for (const storeName of Object.keys(parsedData)) {
      /* Store doesn't exist, pass */
      if (!this.dbObjectStores.includes(storeName)) {
        promises.push(
          Promise.reject(
            new Error(`Ignoring unknown object store ${storeName}`)
          )
        );
        continue;
      }

      /* Import store */
      const store = transaction.objectStore(storeName);
      parsedData[storeName].forEach((record) => {
        promises.push(
          new Promise((resolve, reject) => {
            const request = store.put(record);
            request.onsuccess = () => resolve();
            request.onerror = (event) => reject(event.target.error);
          })
        );
      });
    }

    let responses;
    try {
      responses = await Promise.allSettled(promises);

      await new Promise((resolve, reject) => {
        transaction.oncomplete = resolve;
        transaction.onerror = (event) => reject(event.target.error);
      });
    } catch (error) {
      throw new Error("Data importing failed", { cause: error });
    }

    /* Partial errors */
    const rejectedPromiseErrors = responses
      .filter((response) => response.status === "rejected")
      .map((response) => response.reason);
    if (rejectedPromiseErrors.length > 0)
      throw new Error(`Failed to import ${rejectedPromiseErrors} stores`, {
        cause: rejectedPromiseErrors
      });
  }
}

/*
 * Initialize LupBookDB after a page load
 */
window.addEventListener("DOMContentLoaded", async () => {
  await LupBookDB.getDatabase();
});
