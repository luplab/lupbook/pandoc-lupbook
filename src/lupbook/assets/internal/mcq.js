/*
 * Copyright (c) 2023 LupLab
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

class MCQActivity extends LupBookStandardActivity {
  /* Class members */
  choiceItems = [];
  testingScore;
  feedbackItems = [];

  totalCorrectCount = 0;
  testingCount = 0;
  selectedCount = 0;

  /* Class methods */
  constructor(elt) {
    super("mcq", elt);

    /* Handles on various elements of our activity */
    this.choiceItems = Array.from(
      elt.getElementsByClassName("form-check-input")
    );
    this.testingScore = document.getElementById(
      `${this.prefixId}-testing-score`
    );
    this.feedbackItems = Array.from(
      elt.getElementsByClassName("mcq-feedback-item")
    );
  }

  async initActivity() {
    await super.initActivity();

    this.choiceItems.forEach((item) => {
      /* Count number of correct items total */
      if (item.dataset.correct !== undefined) this.totalCorrectCount++;

      /* Install click listener */
      item.addEventListener("click", () => {
        if (item.checked) this.selectedCount++;
        else this.selectedCount--;
        this.updateResetSubmitStatus();
        this.saveActivity();
      });
    });

    this.testingCount =
      this.choiceItems[0].type == "radio" ? 1 : this.choiceItems.length;

    await this.loadActivity();
    this.updateResetSubmitStatus();
  }

  onReset() {
    this.selectedCount = 0;
    this.choiceItems.forEach((item) => {
      item.checked = false;
    });

    /* No new submission until an item is clicked */
    this.setResetSubmitStatus(false);

    /* Clear testing area */
    this.visibilityProgress(false);
    this.hideFeedback();

    this.hideTestingScore();
    this.hideTestingFeedback();
    this.clearTestingScore();
    this.clearFeedbackItems();
  }

  onSubmit() {
    let userSelected = 0,
      userCorrect = 0;

    /* Disable buttons */
    this.setResetSubmitStatus(false);

    /* Clear info from previous submission if any */
    this.clearProgress();
    this.clearTestingScore();
    this.clearFeedbackItems();

    /* Now compute which choice items are correct */
    this.choiceItems.forEach((item, idx) => {
      const feedbackItem = this.feedbackItems[idx];

      /* Show feedback only if choice item was selected */
      if (item.checked == true) {
        userSelected++;

        /* Show corresponding feedback item and color it appropriately */
        feedbackItem.classList.remove("d-none");
        if (item.dataset.correct !== undefined) {
          feedbackItem.classList.add("border-success");
          userCorrect++;
        } else {
          feedbackItem.classList.add("border-danger");
        }
      } else {
        feedbackItem.classList.add("d-none");
      }
    });

    /* Overall success */
    let success =
      userSelected == this.totalCorrectCount &&
      userCorrect == this.totalCorrectCount;

    /* Set up progress bar */
    this.updateProgress(this.testingCount, userCorrect);

    /* Feedback score */
    if (success) {
      this.testingScore.classList.add("alert-success");
      this.testingScore.textContent = "Congratulations!";
    } else {
      this.testingScore.classList.add("alert-danger");

      if (this.choiceItems[0].type == "radio")
        this.testingScore.textContent = "Incorrect answer.";
      else
        this.testingScore.textContent = `You selected ${userSelected} items: ${userCorrect} out of ${this.totalCorrectCount} correct items and ${userSelected - userCorrect} incorrect items.`;
    }

    /* Show feedback */
    this.testingScore.classList.remove("d-none");
    this.showFeedback();

    /* Overall feedback via submit button */
    let s = success
      ? LupBookActivity.SubmitStatus.SUCCESS
      : LupBookActivity.SubmitStatus.FAILURE;
    this.submitStatus(s);
    this.resetStatus(true);

    /* Save score */
    this.saveActivity(userCorrect);
  }

  updateResetSubmitStatus() {
    const resetSubmitStatusFlag = this.selectedCount >= 1;
    this.setResetSubmitStatus(resetSubmitStatusFlag);
  }

  async saveActivity(correctCount) {
    const data = {
      id: this.prefixId,
      items: this.choiceItems.map((item) => item.checked),
      score: correctCount
    };

    await this.saveData(data);
  }

  async loadActivity() {
    const data = await this.loadData();
    if (!data) return;

    /* Restore selections */
    this.choiceItems.forEach((item, idx) => {
      item.checked = data.items[idx];
      if (item.checked) this.selectedCount++;
    });

    /* Restore score if any */
    if (data.score !== undefined)
      this.updateProgress(this.testingCount, data.score);
  }
}

/*
 * Initialize "MCQ" interactive activities after page loading
 */
window.addEventListener("DOMContentLoaded", async () => {
  let mcqActivities = [];

  for (const e of document.getElementsByClassName("mcq-container")) {
    const mcqActivity = new MCQActivity(e);
    await mcqActivity.initActivity();
    mcqActivities.push(mcqActivity);
  }
});
