/*
 * Copyright (c) 2024 LupLab
 * SPDX-License-Identifier: AGPL-3.0-only
 */

class ThemeManager {
  autoThemeListener = null;

  constructor() {
    this.htmlElement = document.documentElement;

    /* Retrieve the user's theme preference or default to 'auto' */
    const savedTheme = this.getThemeFromStorage() || 'auto';

    /* Apply the initial theme */
    this.setTheme(savedTheme);

    /* Attach event listeners to theme buttons */
    this.attachEventListeners();
  }

  /* Apply the selected theme */
  setTheme(theme) {
    /* Save the user's preference */
    this.saveThemeToStorage(theme);

    /* Update the UI to reflect the selected theme */
    this.updateCheckmarks(theme);

    if (theme === 'auto') {
      this.applySystemTheme();
      this.setupAutoThemeListener();
    } else {
      this.removeAutoThemeListener();
      this.applyTheme(theme);
    }
  }

  getSystemTheme() {
    return window.matchMedia('(prefers-color-scheme: dark)');
  }

  /* Apply the system's preferred theme */
  applySystemTheme() {
    const systemTheme = this.getSystemTheme().matches ? 'dark' : 'light';
    this.applyTheme(systemTheme);
  }

  /* Apply the given theme to the document */
  applyTheme(theme) {
    this.htmlElement.setAttribute('data-bs-theme', theme);
  }

  /* Setup a listener for system theme changes in 'auto' mode */
  setupAutoThemeListener() {
    if (this.autoThemeListener) {
      return;
    }
    const query = this.getSystemTheme();
    const listener = () => this.applySystemTheme();
    query.addEventListener('change', listener);
    this.autoThemeListener = { query, listener };
  }

  /* Remove the system theme change listener */
  removeAutoThemeListener() {
    if (!this.autoThemeListener) {
      return;
    }
    this.autoThemeListener.query.removeEventListener('change', this.autoThemeListener.listener);
    this.autoThemeListener = null;
  }

  /* Attach event listeners to the theme buttons */
  attachEventListeners() {
    this.addButtonListener('lb-theme-dark', 'dark');
    this.addButtonListener('lb-theme-light', 'light');
    this.addButtonListener('lb-theme-auto', 'auto');
  }

  /* Add a click listener to a theme button */
  addButtonListener(buttonId, theme) {
    document.getElementById(buttonId).addEventListener('click', () => this.setTheme(theme));
  }

  /* Update the checkmarks to reflect the active theme */
  updateCheckmarks(activeTheme) {
    const themes = ['auto', 'light', 'dark'];

    themes.forEach((theme) => {
      const checkmark = document.getElementById(`lb-theme-${theme}-check`);
      if (checkmark) {
        checkmark.classList.toggle('d-none', theme !== activeTheme);
      }
    });
  }

  /* Retrieve the theme from storage */
  getThemeFromStorage() {
    // TODO: Call saving API
  }

  /* Save the theme to storage */
  saveThemeToStorage(theme) {
    // TODO: Call saving API
  }
}

/* Initialize Theme Manager after page load */
window.addEventListener('DOMContentLoaded', () => {
  new ThemeManager();
});
