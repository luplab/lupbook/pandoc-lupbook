/*
 * Copyright (c) 2023 LupLab
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

class MatchingActivity extends LupBookStandardActivity {
  /* Class members */
  choiceBox;
  choiceItems = [];
  answerContainer;
  answerBoxes = [];
  testingScore;
  feedbackItems = [];

  placeHolder;

  /* Class methods */
  constructor(elt) {
    super("matching", elt);

    /* Handle on various elements of our activity */
    this.choiceBox = document.getElementById(`${this.prefixId}-choices`);
    this.choiceItems = Array.from(
      this.choiceBox.getElementsByClassName("matching-choice")
    );
    this.answerContainer = document.getElementById(`${this.prefixId}-answers`);
    this.answerBoxes = Array.from(
      elt.getElementsByClassName("matching-answer")
    );
    this.testingScore = document.getElementById(
      `${this.prefixId}-testing-score`
    );
    this.feedbackItems = Array.from(
      elt.getElementsByClassName("matching-feedback-item")
    );

    /* Placeholder box we can dynamically display when placing choices */
    this.placeHolder = document.createElement("div");
    this.placeHolder.className =
      "matching-placeholder border border-0 bg-secondary-subtle rounded m-2 mb-0 p-2 d-flex";
  }

  async initActivity() {
    await super.initActivity();

    /* Attach "source" dragging functions to choice items */
    this.choiceItems.forEach((item) => {
      item.draggable = true;

      item.ondragstart = (event) => {
        event.dataTransfer.clearData();
        event.dataTransfer.setData("text", event.target.id);
        event.dataTransfer.effectAllowed = "move";
        event.target.classList.replace("bg-body", "bg-body-tertiary");

        setTimeout(() => {
          this.answerBoxes.forEach((box) => {
            box.appendChild(this.placeHolder.cloneNode());
          });
        }, 0);
      };

      item.ondragend = (event) => {
        event.target.classList.replace("bg-body-tertiary", "bg-body");

        setTimeout(() => {
          Array.from(
            this.answerContainer.getElementsByClassName("matching-placeholder")
          ).forEach((node) => node.remove());
        }, 0);
      };
    });

    /* Attach "target" dragging functions to answer boxes */
    this.answerBoxes.forEach((box) => {
      box.ondragover = (event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = "move";

        /* Highlight placeholder we're onto if any */
        Array.from(
          this.answerContainer.getElementsByClassName("matching-placeholder")
        ).forEach((node) => {
          if (node === event.target)
            node.classList.replace("bg-secondary-subtle", "bg-primary-subtle");
          else
            node.classList.replace("bg-primary-subtle", "bg-secondary-subtle");
        });
      };

      box.ondrop = (event) => {
        event.preventDefault();

        /* Only dropping in a placeholder */
        if (!event.target.classList.contains("matching-placeholder")) return;

        /* Target answer box */
        const box = event.target.closest(".matching-answer");

        /* Move dragged element to target container */
        const dragged = document.getElementById(
          event.dataTransfer.getData("text")
        );
        box.insertBefore(dragged, event.target);

        /* Modifications re-enable the activity's submittability */
        this.submitStatus(LupBookActivity.SubmitStatus.ENABLED);
        this.resetStatus(true);

        this.saveActivity();
      };
    });

    /* Move choice items back to choice box in one click */
    this.choiceItems.forEach((item) => {
      item.addEventListener("click", () => {
        if (item.parentNode !== this.choiceBox) {
          this.choiceBox.appendChild(item);
          this.updateResetSubmitStatus();
          this.saveActivity();
        }
      });
    });

    await this.loadActivity();
    this.updateResetSubmitStatus();
  }

  onReset() {
    /* Move all choice items back to choice box */
    this.choiceItems.forEach((item) => this.choiceBox.appendChild(item));

    /* No new submission until an item is moved over */
    this.setResetSubmitStatus(false);

    /* Clear testing area */
    this.visibilityProgress(false);
    this.hideFeedback();

    this.hideTestingScore();
    this.hideTestingFeedback();
    this.clearTestingScore();
    this.clearFeedbackItems();

    this.saveActivity();
  }

  onSubmit() {
    const choiceCount = this.choiceItems.length;
    let correctCount = 0;

    /* Disable buttons */
    this.setResetSubmitStatus(false);

    /* Clear info from previous submission if any */
    this.clearProgress();
    this.clearTestingScore();
    this.clearFeedbackItems();

    /* Now compute which choice items are correct */
    this.choiceItems.forEach((item, idx) => {
      const choiceContainerElt = item.parentNode;
      const feedbackItem = this.feedbackItems[idx];
      const answerChoices = choiceContainerElt.dataset.choices
        ? choiceContainerElt.dataset.choices.split(",")
        : [];

      const choiceId = item.id.split("-").pop();
      if (
        feedbackItem !==
        document.getElementById(`${this.prefixId}-feedback-${choiceId}`)
      )
        throw new Error("ohoh");

      /* Show feedback only if choice item was moved to answer box */
      if (choiceContainerElt.classList.contains("matching-answer")) {
        /* Show corresponding feedback item and color it appropriately */
        feedbackItem.classList.remove("d-none");
        if (answerChoices.includes(choiceId)) {
          feedbackItem.classList.add("border-success");
          correctCount++;
        } else {
          feedbackItem.classList.add("border-danger");
        }
      } else {
        feedbackItem.classList.add("d-none");
      }
    });

    /* Set up progress bar */
    this.updateProgress(this.choiceItems.length, correctCount);

    /* Feedback score */
    if (correctCount == choiceCount) {
      this.testingScore.textContent = "Congratulations!";
      this.testingScore.classList.add("alert-success");
    } else {
      this.testingScore.textContent = `You correctly matched ${correctCount} items out of ${choiceCount}.`;
      this.testingScore.classList.add("alert-danger");
    }

    /* Show feedback */
    this.testingScore.classList.remove("d-none");
    this.showFeedback();

    /* Overall feedback via submit button */
    let s =
      correctCount == choiceCount
        ? LupBookActivity.SubmitStatus.SUCCESS
        : LupBookActivity.SubmitStatus.FAILURE;
    this.submitStatus(s);
    this.resetStatus(true);

    this.saveActivity(correctCount);
  }

  updateResetSubmitStatus() {
    const resetSubmitStatusFlag = this.choiceItems.some(
      (item) => item.parentNode !== this.choiceBox
    );
    this.setResetSubmitStatus(resetSubmitStatusFlag);
  }

  async saveActivity(correctCount) {
    const map = {};
    this.answerBoxes.forEach((answerBox) => {
      answerBox.querySelectorAll(".matching-choice").forEach((choiceItem) => {
        map[choiceItem.id] = answerBox.id;
      });
    });

    const data = {
      id: this.prefixId,
      items: map,
      score: correctCount
    };
    await this.saveData(data);
  }

  async loadActivity() {
    const data = await this.loadData();
    if (!data) return;

    /* Restore item placement */
    for (const [choiceId, answerId] of Object.entries(data.items)) {
      const choice = document.getElementById(choiceId);
      const answerBox = document.getElementById(answerId);

      if (choice && answerBox) answerBox.appendChild(choice);
      else console.warn(`Unknown element with id ${choiceId} or ${answerId}`);
    }

    /* Restore score if any */
    if (data.score !== undefined)
      this.updateProgress(this.choiceItems.length, data.score);
  }
}

/*
 * Initialize "matching" interactive components after page loading
 */
window.addEventListener("DOMContentLoaded", async () => {
  let matchingActivities = [];

  for (const e of document.getElementsByClassName("matching-container")) {
    const matchingActivity = new MatchingActivity(e);
    await matchingActivity.initActivity();
    matchingActivities.push(matchingActivity);
  }
});
