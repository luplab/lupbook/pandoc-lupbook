/*
 * Copyright (c) 2023 LupLab
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

class FIBActivity extends LupBookStandardActivity {
  /* Class members */
  blankItems = [];
  testingScore;
  feedbackItems = [];

  filledCount = [];
  casing;

  /* Class methods */
  constructor(elt) {
    super("fib", elt);

    /* Handles on various elements of our activity */
    this.blankItems = Array.from(elt.getElementsByClassName("form-control"));
    this.testingScore = document.getElementById(
      `${this.prefixId}-testing-score`
    );
    this.feedbackItems = Array.from(
      elt.getElementsByClassName("fib-feedback-item")
    );
    const casingDiv = document.getElementById(`${this.prefixId}-casing-div`);

    /* Instance members */
    this.casing = casingDiv.dataset.casing;
    this.filledCount = Array(this.blankItems.length).fill(false);
  }

  async initActivity() {
    await super.initActivity();

    this.blankItems.forEach((item, idx) => {
      item.addEventListener("input", () => {
        this.setFilledCount(item, idx);
        this.updateResetSubmitStatus();
        this.saveActivity();
      });
    });

    await this.loadActivity();
    this.updateResetSubmitStatus();
  }

  onReset() {
    /* Reset all fills */
    this.filledCount.fill(false);
    this.blankItems.forEach((item) => (item.value = ""));

    /* No new submission until all blanks are filled out */
    this.setResetSubmitStatus(false);

    /* Clear testing area */
    this.visibilityProgress(false);
    this.hideFeedback();

    this.hideTestingScore();
    this.hideTestingFeedback();
    this.clearTestingScore();
    this.clearFeedbackItems();

    this.saveActivity();
  }

  onSubmit() {
    const blankCount = this.blankItems.length;
    let correctCount = 0;

    /* Disable buttons */
    this.setResetSubmitStatus(false);

    /* Clear info from previous submission if any */
    this.clearProgress();
    this.clearTestingScore();
    this.clearFeedbackItems();

    /* Now compute which blank items are correct */
    this.blankItems.forEach((item, idx) => {
      const feedbackItem = this.feedbackItems[idx];

      /* Show corresponding feedback item and color it appropriately */
      feedbackItem.classList.remove("d-none");

      /* By default, case sensitive */
      let itemVal = item.value;
      let answerVal = item.dataset.answer;

      /* Unless specified otherwise */
      if (this.casing === false) {
        itemVal = itemVal.toLowerCase();
        answerVal = answerVal.toLowerCase();
      }

      if (itemVal === answerVal) {
        feedbackItem.classList.add("border-success");
        correctCount++;
      } else {
        feedbackItem.classList.add("border-danger");
      }
    });

    /* Set up progress bar */
    this.updateProgress(this.blankItems.length, correctCount);

    /* Feedback score */
    if (correctCount == blankCount) {
      this.testingScore.textContent = "Congratulations!";
      this.testingScore.classList.add("alert-success");
    } else {
      this.testingScore.textContent = `You correctly filled in ${correctCount} blanks out of ${blankCount}.`;
      this.testingScore.classList.add("alert-danger");
    }

    /* Show feedback */
    this.testingScore.classList.remove("d-none");
    this.showFeedback();

    /* Overall feedback via submit button */
    let s =
      correctCount == blankCount
        ? LupBookActivity.SubmitStatus.SUCCESS
        : LupBookActivity.SubmitStatus.FAILURE;
    this.submitStatus(s);
    this.resetStatus(true);

    /* Save score after grading */
    this.saveActivity(correctCount);
  }

  setFilledCount(item, idx) {
    if (item.value != "") this.filledCount[idx] = true;
    else this.filledCount[idx] = false;
  }

  updateResetSubmitStatus() {
    const resetStatusFlag = this.filledCount.some(Boolean);
    const submitStatusFlag = this.filledCount.every(Boolean);
    this.setResetSubmitStatus(resetStatusFlag, submitStatusFlag);
  }

  async saveActivity(correctCount) {
    const data = {
      id: this.prefixId,
      items: this.blankItems.map((item) => item.value),
      score: correctCount
    };
    await this.saveData(data);
  }

  async loadActivity() {
    const data = await this.loadData();
    if (!data) return;

    /* Restore blanks */
    this.blankItems.forEach((item, idx) => {
      item.value = data.items[idx];
      this.setFilledCount(item, idx);
    });

    /* Restore score if any */
    if (data.score !== undefined)
      this.updateProgress(this.blankItems.length, data.score);
  }
}

/*
 * Initialize "FIB" interactive activities after page loading
 */
window.addEventListener("DOMContentLoaded", async () => {
  let fibActivities = [];

  for (const e of document.getElementsByClassName("fib-container")) {
    const fibActivity = new FIBActivity(e);
    await fibActivity.initActivity();
    fibActivities.push(fibActivity);
  }
});
