# Lupbook framework

## Get LupBook-VM

Get precompiled `lupbookvm.js` file from
https://gitlab.com/luplab/lupbook/lupbook-vm/-/jobs/artifacts/main/file/build/lupbookvm.js?job=build
or compile it manually.

## Compile internal documentation

```
$ python -m venv venv
$ source venv/bin/activate
$ pip install -e .
$ cd doc
$ lupbook --lupbookvmjs-file /path/to/lupbookvm.js
```

## Compile your own book

```
$ cd /path/to/book
$ python -m venv venv
$ source venv/bin/activate
$ pip install -e /path/to/lupbook
$ lupbook --lupbookvmjs-file /path/to/lupbookvm.js
```
