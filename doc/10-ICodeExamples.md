# Interactive code (ICode) examples

## Simplest Example

Example with program from scratch and no output checking.

``` icode
id: write-any-program
title: Free writing
prompt: >
    Write any program you want, and run it to see its output. It should exit
    with `0` though.
skeleton:
  - filename: main.c
tests:
  - name: build
    fatal: true
    cmd: gcc main.c
  - name: run
    cmd: ./a.out
```

## Starter code and output checking

Example with started code that students need to complete. We check the program's
output.

``` icode
id: simple-example-skel
title: Hello world
prompt: >
    Finish the program below so that it outputs the following to stdout:
    ```
    Hello world!\n
    ```
skeleton:
  - filename: main.c
    data: |
      #include <stdio.h>

      int main(void)
      {
          printf("toto");

          return 0;
      }
  - filename: tester.sh
    hidden: true
    data: |
      render_visible_chars() {
          local input="$1"
          echo -ne "$input" | cat -A
      }
      # Add temporary `.` at the end so that trailing newlines don't get stripped
      output=$(./a.out; echo .)
      output="${output%.}"
      expected=$'Hello world!\n'
      if [[ "${output}" != "${expected}" ]]; then
          echo "Output does not match the expected result."
          exp_render=$(render_visible_chars "${expected}")
          out_render=$(render_visible_chars "${output}")
          echo "Expected: '${exp_render}'"
          echo "Received: '${out_render}'"
          exit 1
      fi
      echo "Output matches the expected result!"
      exit 0
tests:
  - name: build
    fatal: true
    cmd: gcc main.c
  - name: correct output
    cmd: sh tester.sh
```

## Complex example

By hiding some of the input files from the user, you can design complex test
cases without necessarily exposing this code as part of the exercise.

It is also possible to make input files readonly, or restrict editing to certain
lines.

``` icode
!include complex_example/icode.yaml
```
